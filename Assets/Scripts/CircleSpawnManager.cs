﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class CircleSpawnManager : MonoBehaviour
{
    private const string CIRCLE_PREFAB_PATH = "Assets/Prefabs/Circle.prefab";
    private const string BLACK_CIRCLE_PREFAB_PATH = "Assets/Prefabs/BlackCircle.prefab";
    public GameObject circlePrefab;
    public GameObject blackCirclePrefab;
    private GameObject canvas;
    public float width = 360;// 
    public float height = 160;
    private float timer;
    private float spawnTimeInterval = 1;
    private float spawnTime;
    private int lvl = 1;
    
    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.Find("CircleCanvas");
        Instantiate(circlePrefab, canvas.transform);
    }

    public int getLvl()
    {
        return lvl;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        spawnTime += Time.deltaTime;
        if (timer >= spawnTimeInterval)
        {
            timer = 0;
            Vector3 newCircleCoord = getRandomCoordinates();
            if (Random.value >= 0.1f)
            {    
                GameObject circle = Instantiate(circlePrefab, canvas.transform);
                circle.transform.localPosition = newCircleCoord;
            }
            else
            {
                GameObject circle = Instantiate(blackCirclePrefab, canvas.transform);
                circle.transform.localPosition = newCircleCoord;
            }
        }

        if (spawnTime >= 5)
        {
            spawnTime = 0;
            lvl = lvl == 6 ? lvl : lvl + 1;
            if (lvl == 2)
            {
                spawnTimeInterval = 0.7f;
            }

            if (lvl == 3)
            {
                spawnTimeInterval = 0.5f;
            }

            if (lvl == 4)
            {
                spawnTimeInterval = 0.4f;
            }

            if (lvl == 5)
            {
                spawnTimeInterval = 0.35f;
            }

            if (lvl == 6)
            {
                spawnTimeInterval = 0.3f;
            }
        }

        
    }

    //It doesn't work properly and I'm not sure why.
    Vector3 getRandomCoordinates()
    {
        while (true)
        {
            float x = Random.value * 2 * width - width;
            float y = Random.value * 2 * height - height;
            Vector3 resCand = new Vector3(x,y,0);
            if (Physics2D.OverlapCircle(new Vector2(x,y), 200, 1<<5))
            {           
            }
            else
            {
                return resCand;
            }
        }
    }
    
}
