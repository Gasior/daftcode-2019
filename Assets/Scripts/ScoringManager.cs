﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoringManager : MonoBehaviour
{
    public GameObject TimeValueLabel;
    private Text timeText;
    private float time;
    private bool defeated;
    public GameObject MainMenuButton;
    public GameObject GameOverLabel;
    public GameObject ScoreLabel;
    public GameObject ScoreValueLabel;
    public GameObject NewHighScoreLabel;
    public GameObject CircleCanvas;
    
    // Start is called before the first frame update
    void Start()
    {
        timeText = TimeValueLabel.GetComponent<Text>();
    }

    // Update is called once per frame
    void OnGUI()
    {
        if (!defeated)
        {
            time += Time.deltaTime;
            timeText.text = Math.Round(time, 3).ToString();
        }
    }

    public void onDefeat()
    {
        if(defeated)
            return;
        CircleCanvas.SetActive(false);
        defeated = true;
        Destroy(GameObject.Find("CircleSpawner"));
        float currHighScore = PlayerPrefs.GetFloat("highScore", 0);        

        GameOverLabel.SetActive(true);
        ScoreLabel.SetActive(true);
        ScoreValueLabel.GetComponent<Text>().text = time.ToString();
        ScoreValueLabel.SetActive(true);
        MainMenuButton.SetActive(true);
        if (currHighScore < time)
        {
            PlayerPrefs.SetFloat("highScore", time);
            NewHighScoreLabel.SetActive(true);
        }

    }
}
