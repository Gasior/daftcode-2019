﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.EventSystems;

public class BlackCircleControler : CircleLifeController, IPointerClickHandler
{

    void Start()
    {
        init();
        lifetime = 3;
    }
    
    void Update()
    {
        spawnTime += Time.deltaTime;
        if (spawnTime >= lifetime)
        {
            Destroy(gameObject);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        sm.onDefeat();
    }
}
