﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreDisplay : MonoBehaviour
{
    public Text highScoreText;

    private void Start()
    {
        highScoreText.text = PlayerPrefs.GetFloat("highScore", 0).ToString();
    }
}
