﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NormalCircleLifeController : CircleLifeController, IPointerClickHandler
{
    private Image redCircle;

    // Start is called before the first frame update
    void Start()
    {
        init(); 
        redCircle = gameObject.transform.Find("RedCircle").gameObject.GetComponent(typeof(Image)) as Image;
        int lvl = cm.getLvl();
        if (lvl == 1)
        {
            lifetime = Random.Range(2f, 4f);
        }

        if (lvl == 2)
        {
            lifetime = Random.Range(2.0f, 3.5f);
        }

        if (lvl == 3)
        {
            lifetime = Random.Range(1.5f, 2.0f);
        }

        if (lvl == 4)
        {
            lifetime = Random.Range(1f, 2.0f);
        }

        if (lvl == 5)
        {
            lifetime = Random.Range(0.7f, 1.6f);
        }

        if (lvl == 6)
        {
            lifetime = Random.Range(0.6f, 1.4f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        spawnTime += Time.deltaTime;
        if (spawnTime <= lifetime)
        {
            redCircle.fillAmount += 1.0f / lifetime * Time.deltaTime;
        }
        else
        {
            Debug.Log("dead");
            sm.onDefeat();
        }
    }
    
    

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("destroy");
        Destroy(gameObject);
    }
}
